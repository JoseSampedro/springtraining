package com.privalia.entity;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {

	public static void main(String[] args) {
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
		
		Student joeStudent = (Student)context.getBean("joeStudent");
		
		System.out.println(joeStudent.getName());
		
		Student joeStudentConst = (Student)context.getBean("joeStudentConstructor");
		
		System.out.println(joeStudentConst.getSurname());
	
		context.close();
	}
}
