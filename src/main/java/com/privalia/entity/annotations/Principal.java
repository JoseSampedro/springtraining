package com.privalia.entity.annotations;

//import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Principal {
	public static void main(String[] args) {
		//AnnotationConfigApplicationContext annotationContext = new AnnotationConfigApplicationContext();
		
		//annotationContext.scan("com.privalia.entity.annotations");
		//annotationContext.refresh();
		
		//Student peterStudent = (Student)annotationContext.getBean("peterStudent");
		//System.out.println(peterStudent.getName());
		
		AbstractApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
		Student bruceStudent = (Student)context.getBean("bruceWayneStudent");
		
		StringBuilder sb = new StringBuilder();
		sb.append(bruceStudent.getIdStudent());
		sb.append(" ");
		sb.append(bruceStudent.getName());
		sb.append(" ");
		sb.append(bruceStudent.getSurname());
		sb.append(" ");
		sb.append(bruceStudent.getAge());
		sb.append(" ");
		sb.append(bruceStudent.getAddress().getName());
		
		System.out.println(sb.toString());
		
		//annotationContext.close();
		context.close();
	}
}
