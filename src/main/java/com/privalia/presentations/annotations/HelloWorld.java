package com.privalia.presentations.annotations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

/*
 * @Repository: Used to mark a bean as DAO component on persistence layer.
 * @Service: Used to mark a bean as Service Component on business layer.
 * @Controller: Used to mark a bean as a Controller component on presentation layer.
 * @Configuration: Used to mark a bean as Configuration component.
 * @Component: General purpose annotation, can be used as a replacement for above.
 * 
 * http://www.baeldung.com/spring-beans-scopes (El default scope de una bean es Singleton)
 * http://www.javapoint.com/dependency-injection-with-factory-method
 */
public class HelloWorld {

	@Autowired
	@Value("Hello World from Annotations!")
	private String hello;
	
	public String getHello() {
		return hello;
	}
}
