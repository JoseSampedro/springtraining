package com.privalia.presentations.annotations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/*
 * Utiliza código para instanciar la dependencia
 */
@Configuration
public class SpringConfiguration {
	
	@Bean(name="helloWorld")
	public HelloWorld helloWorld() {
		return new HelloWorld();
	}
}
