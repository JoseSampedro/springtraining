package com.privalia.presentations.annotations;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

public class Principal {

	public static void main(String[] args) {
		AbstractApplicationContext appContext = new AnnotationConfigApplicationContext(
				SpringConfiguration.class); // Sólo podemos pasarle clases que implementan @Configuration
		HelloWorld helloWorld = (HelloWorld)appContext.getBean("helloWorld");
		System.out.println(helloWorld.getHello());
		
		appContext.close();
	}
}
