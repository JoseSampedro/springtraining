package com.privalia.presentation;

public class HelloWorld {
	private String hello;

	/**
	 * @return the hello
	 */
	public String getHello() {
		return hello;
	}

	/**
	 * @param hello the hello to set
	 */
	public void setHello(String hello) {
		this.hello = hello;
	}
	
	/*
	 * Creamos los constructores porque al usar reflexión en inyección de 
	 * dependencias, van a llamarlos.
	 */
	public HelloWorld() {
	}
	
	public HelloWorld(String hello) {
		this.hello = hello;
	}
}
